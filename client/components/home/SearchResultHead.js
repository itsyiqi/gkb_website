/*
 * SearchResultHead is a UI component, only render the data sent from SearchResultList
 */

import React from 'react';
import { connect } from 'react-redux';
import place from '../img/ic-place-black-48-dp.png';
import favour_icon from '../img/icons8-heart-filled-50.png';
import share_icon from '../img/icons8-share-filled-50.png';
import nearby_icon from '../img/icons8-mind-map-50.png';
import direction_icon from '../img/icons8-waypoint-map-filled-50.png';
import {Link} from 'react-router';
import config from '../../../server/config.js';
import Nearby from "../navigation/Nearby";
import ShowDirections from "../navigation/ShowDirections";

class SearchResultHead extends React.Component{

    constructor(props){
        super(props);
        console.log("Current search location: ", this.props);
        this.state = {
          searchStr: "",
          errors: {}
        }

        this.addFavorite = this.addFavorite.bind(this);
        this.closeSearchResult = this.closeSearchResult.bind(this);
    }

    nearby(e){
      if (!this.props.showNearby) {
      var osm_id = this.props.login.user.osm_id;
      var osm_type = this.props.login.user.osm_type;
      $.ajax({
         url: 'http://115.146.90.170:8080/nearestLandmarks?location='+osm_type +':'+ osm_id,
         data: {
            format: 'json'
         },
         error: function(xhr,status,error) {
          var err = eval("("+xhr.responseText+ ")");
         },
         context:this,
         success: function(data) {
           this.props.setShowNearby(true, data.landmarks, "");
	 },
         type: 'GET'
      });
      } else {
          this.props.setShowNearby(false, "", "");
      }
    }

    addFavorite(e) {
        console.log("In function addFavorite");
        console.log("In add favorites: location: ", this.props.location)
        console.log("this.props.searchResult: ", this.props.searchResult);
        var location = this.props.location;
        var photo = this.props.photo
        const {user} = this.props.login;
        var  autoComment = this.props.autoComment
        var description = {
            location : location,
            photo : photo,
            coords: user.coords,
            user_id: user._id,
            type: this.props.searchResult.searchResultPageConfig.type,
            autoComment: autoComment
        }

        this.props.addToFavoritesAction(description).then(
            (res) => {
                console.log("we are back in addToFavoritesAction clientside");
            },
            // if server response any error message, set it into state errors
            (err) => {
                console.log("addFavorite failed");
                this.setState({ errors: err.response.data});
                console.log("this.state.errors: ", this.state.errors);
            });
    }

    closeSearchResult(e){
        this.props.hideSearchResult();
    }

    render(){
        const location = this.props.location;
        var imgSrc = this.props.photo;
        // print props
        console.log("props: ",this.props);
        if(imgSrc == '' || imgSrc == null){
            imgSrc = "http://www.mozmagic.com/files/assets/img/ui/no-image-available.png"
        } else if (imgSrc.indexOf("https") < 0) {
            // result is from google place photo => photo_ref
            imgSrc = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + imgSrc + "&key=" + config.googlePlaceApiKey
        }

        return(
        <div className="row">
            <div id="search-result-modal" className="col-lg-12 search-result-bar">
		<div className="row">
		    <div className="col-lg-12 photo-tag">
		        <div className="row">
		            <div className="col-lg-12">
		                <button type="button" className="close" onClick={this.closeSearchResult}>&times;</button>
		            </div>
		        </div>

		        <div className="row">
                            <img className="col-md-6 col-lg-6" src={imgSrc}/>
                            <div className="col-md-6 col-lg-6 place-sec" id="style-1">
                                <img className="small-icon-rec icon-place" src={place}/>
                                <p>{location}</p>
                            </div>
                        </div>

		        <div className="row">
		            <div className="col-lg-12 features-btn">
                                <img className="small-icon-sq" src={favour_icon} />
                                <a onClick={this.addFavorite} href='#'>Favourite</a>

                                <img className="small-icon-sq" src={share_icon}/>
                                <a href='#'>Share</a>

                                <img className="small-icon-sq" src={nearby_icon}/>
                                <a onClick={this.nearby.bind(this)} href='#'> Nearby</a>

                                <img className="small-icon-sq" src={direction_icon}/>
                                <a onClick={this.props.showDirectionSearch} href='#'>Direction</a>
		           </div>
		        </div>
                    </div>
                </div>

                <div className="row">
		    <div className="col-lg-12">
                    { this.props.showNearby &&
		    < Nearby
		    	landmarks = {this.props.landmarks}
		    	nearbyDirection = {this.props.nearbyDirection}
		    	requestPath = {this.props.requestPath}
		    	setShowNearby = {this.props.setShowNearby}
		    	id= {this.props.login.user.osm_id}
		    	type = {this.props.login.user.osm_type}
		    />
		    }
		    </div>
                </div>
            </div>
        </div>
        );
    }
}

SearchResultHead.propTypes = {
    addToFavoritesAction: React.PropTypes.func.isRequired,
    login: React.PropTypes.object.isRequired,
}

SearchResultHead.contextTypes = {
    router: React.PropTypes.object.isRequired
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    return {
        login: state.login,
        searchResult: state.searchResult
    };
}

export default connect(mapStateToProps, null)(SearchResultHead);
