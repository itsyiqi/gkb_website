/*
 * SearchResultList is the container component for SearchResultHead and SearchResultItem
 * It connect to redux store, fetch the newest searchResult State and transfer to SearchResultHead and SearchResultItem
 */

import React from 'react';
import SearchResultItem from './SearchResultItem'
import SearchResultHead from './SearchResultHead'
import addPic from '../img/add-post-button-dark.png';
import AddDescription from './AddDescription';
import { Link } from 'react-router';
import lodash from 'lodash';


class SearchResultList extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            showAddDescription: false
        }
        this.onClickAdd = this.onClickAdd.bind(this);
        this.showAddWindow = this.showAddWindow.bind(this);
        this.hideAddWindow = this.hideAddWindow.bind(this);
    }

    onClickAdd(){
        const isAuthenticated = this.props.isAuthenticated;
        if (isAuthenticated){
            this.showAddWindow();
        }
    }

    showAddWindow(){
        this.setState({
            showAddDescription: true
        });
    }

    hideAddWindow(){
        this.setState({
            showAddDescription: false
        });
    }

    render() {
        var items = [];
        const { placeFullAddr } = this.props.searchResult.searchResultPageConfig;
        const { placePhoto } =this.props.searchResult.searchResultPageConfig;
        const { array }= this.props.descriptionArray;
        const isAuthenticated = this.props.isAuthenticated;

        var addImg;
        if(isAuthenticated){
            addImg = <img id = "add" src = {addPic}
                              onClick = {this.onClickAdd}
            />;
        }else {
            addImg = <Link to="/login" ><img id = "add" src = {addPic}/>
                    </Link>;
        }

        if(!lodash.isEmpty(array)){
            const numList = array.length;
            //console.log("descriptons array:", array);
            for (var i = 0; i < numList; i++) {
                //console.log(resultArray.length);
                const user_id = this.props.user_id;
                const {user_like_array} = array[i].doc;
                var thumbUp = false;
                if(user_like_array != undefined){
                    if(user_like_array.indexOf(user_id) != -1){
                        //console.log("exist");
                        thumbUp = true;
                    }
                }

                items.push(<tr key={i}><td><SearchResultItem userName={array[i].doc.user_name}
                                                             like= {array[i].doc.like}
                                                             num = {i+1}
                                                             des_id = {array[i].doc._id}
                                                             proImg = {array[i].proImg}
                                                             isAuthenticated = {isAuthenticated}
                                                             user_id = {user_id}
                                                             preThumbUp = {thumbUp}
                                                             discription={array[i].doc.description_content}
                                                             updateShowSearchResultLike={this.props.updateShowSearchResultLike}
                                                            />
                    </td></tr>
                );
            }
        }else{
            items.push(<tr key={1}><td><div className = "empty_result_box">
                                    <div className="no-user-submitted-de">No user-submitted descriptions available…</div>
                                    <div className="contribute-your-own">Contribute your own description for this location</div>
                                    <div className="add_box">
                                        {addImg}
                                    </div>
                                </div>
                </td></tr>

            );
        }

        const {autoDescription, sentence} = this.props.login.user;
        console.log('props resultlist',this.props);

        var autoSen;
        if(sentence){
            autoSen = sentence;
        }
        else{
            autoSen = "No sentence is found";
        }

        var autoDesc;
        if (autoDescription) {
          autoDesc = autoDescription;
        } else {
          autoDesc = "No computer generated description available"
        }
	try {
	    var autoComments = autoDesc.match( /[^\.!\?]+[\.!\?]+/g );
	    var wikiSent = autoComments[0];
	    var autoSent = autoComments.slice(1);
	} catch(e) {
	    var wikiSent = autoComment;
	}
	finally{}

        return (
        <div className="row">
            <div className="col-lg-12 no-click-through result_page">
		<div className="row">
		    <div className="col-lg-12">
                        <SearchResultHead
		            autoComment = {autoDesc}
                            location = {placeFullAddr}
                            photo = {placePhoto}
                            addToFavoritesAction = {this.props.addToFavoritesAction}
                            hideSearchResult={this.props.hideSearchResult}
                            searchString={this.props.searchString}
                            requestPath={this.props.requestPath}
                            setShowNearby={this.props.setShowNearby}
                            showNearby={this.props.showNearby}
                            landmarks={this.props.landmarks}
                            nearbyDirection={this.props.nearbyDirection}
                            showDirectionSearch={this.props.showDirectionSearch}
                        />
		    </div>
		</div>

                <div className="row">
		    <div className="col-lg-12 pre-scrollable">
		        <div className="row">
		            <div className="col-lg-12 auto-comment">
		                {wikiSent} <b>{autoSent}</b>
		            </div>
		        </div>

		        <div className="row">
		            <div className="col-lg-12 auto-sentence">
		            {autoSen}
		            </div>
		        </div>

                        <div className="row">
		            <div className="col-lg-12 left-text">
                                <span> Location Descriptions </span>
                                    <div className="add_dis_box" >{addImg}</div>
		            </div>
                        </div>

                        <div className="row">
		            <div className="col-lg-12 scrollit">{items}</div>
                        </div>

		        <div className="row">
		            <div className="col-lg-12">
                            {this.state.showAddDescription &&
		             <AddDescription
		                 hideAddWindow={this.hideAddWindow}
                                 setDescriptionArray = {this.props.setDescriptionArray}
                                 updateShowSearchResult={this.props.updateShowSearchResult}
                                 updateContributionArray = {this.props.updateContributionArray}
                             />
			    }
		            </div>
		        </div>
		    </div>
		</div>
            </div>
        </div>
        )
    }

}

SearchResultList.propTypes = {
    searchResult: React.PropTypes.object.isRequired,
    descriptionArray: React.PropTypes.object.isRequired,
    setDescriptionArray: React.PropTypes.func.isRequired,
    updateShowSearchResult: React.PropTypes.func.isRequired,
    updateShowSearchResultLike: React.PropTypes.func.isRequired,
    addToFavoritesAction: React.PropTypes.func.isRequired,
    updateContributionArray: React.PropTypes.func.isRequired
}


export default SearchResultList;
