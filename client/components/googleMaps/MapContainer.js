/*
 * This component is the top component, it contains the map, and all the other components are under this one.
 */

import React, {Component} from "react"
import { withGoogleMap, GoogleMap, Marker, Polyline, DirectionsRenderer } from "react-google-maps";
import { Router, Route, browserHistory } from 'react-router';
import LoginPage from '../login/LoginPage';
import SignupPage from '../signup/SignupPage';
import ResetPasswordPage from '../resetpwd/ResetPasswordPage';
import EmailSentPage from '../resetpwd/EmailSentPage';
import NewPwdPage from '../newpassword/NewPwdPage';
import WelcomePage from '../welcome/WelcomePage'
import HomePage from '../home/HomePage'
import {connect} from 'react-redux'
import AccountSetting from '../userProfile/AccountSettingPage';
import SearchHistory from '../userProfile/SearchHistoryPage';
import MyFavourites from '../userProfile/FavouritesPage';
import Landing from '../landing/landingPage';
import MyContributions from '../userProfile/ContributionPage';


class MapContainer extends Component {

    constructor(props) {
      super(props);

      this.state = {
        markers: [{
          position: {
            lat: -37.8182711,
            lng: 144.9670618,
          }
        }],
	showDirection: false,
	showNearby: false,
	landmarks: "",
	nearbyDirection: "",
	latitude: -37.8182711,
	longitude: 144.9670618,
      }

      this.requestPath = this.requestPath.bind(this);
      this.requestDirection = this.requestDirection.bind(this);
      this.setSearchLocation = this.setSearchLocation.bind(this);
      this.setShowNearby = this.setShowNearby.bind(this);
      this.setShowDirection = this.setShowDirection.bind(this);
      this.onDirectionsChanged = this.onDirectionsChanged.bind(this);
    }

    componentWillMount() {
      console.log("in componentDidMount woohooo");
      var rest = require('rest');
      var map = this

      rest('http://freegeoip.net/json/').then(function(response) {
          var parsedData = JSON.parse(response.entity)
          map.state = {latitude: parsedData.latitude, longitude: parsedData.longitude}
      });
    }

    requestDirection(dest) {
      const DirectionsService = new google.maps.DirectionsService();
      DirectionsService.route({
	origin: new google.maps.LatLng({ lat:parseFloat(this.state.latitude), lng:parseFloat(this.state.longitude) }),
	destination: new google.maps.LatLng(dest),
	travelMode: google.maps.TravelMode.WALKING,
      }, (result, status) => {
	if (status === google.maps.DirectionsStatus.OK) {
	   this.setState({
	        directions: result,
		showDirection: true,
	   });
	} else {
	   console.error(`error fetching directions ${result}`);
	}
      });
    }

    setShowDirection(show) {
        this.setState({
	    showDirection: show,
	})
    }

    requestPath(dest, nearbyDirection = "") {
      const DirectionsService = new google.maps.DirectionsService();
      DirectionsService.route({
	origin: new google.maps.LatLng({ lat:parseFloat(this.state.latitude), lng:parseFloat(this.state.longitude) }),
	destination: new google.maps.LatLng(dest),
	travelMode: google.maps.TravelMode.WALKING,
      }, (result, status) => {
	if (status === google.maps.DirectionsStatus.OK) {
	   this.setState({
	        directions: result,
                landmarks: "",
		nearbyDirection: nearbyDirection,
	   });
	} else {
	   console.error(`error fetching directions ${result}`);
	}
      });
    }

    setShowNearby(show, details, directions) {
        this.setState({
            showNearby: show,
            landmarks: details,
            nearbyDirection: directions,
	});
    }

    setSearchLocation(lat, lon) {
	this.setState({
	    latitude: lat,
	    longitude: lon,
	    showNearby: false,
	    landmarks: "",
	    nearbyDirection: "",
	    directions: "",
	})
    }

    onDirectionsChanged() {
	this.setState({
	     directions: this.directionsRef.getDirections(),
	});
    }

    render() {
      var lat;
      var longt;
      const {user} = this.props.login
      if (user != null && user.coords != null) {
          console.log("we are here in coords");
          if (typeof(user.coords.lat) == 'string' ) {
              lat = parseFloat(user.coords.lat)
              longt = parseFloat(user.coords.longt)
          } else {
              lat = (user.coords.lat)
              longt = (user.coords.longt)
          }
      } else {
          lat = this.state.latitude;
          longt = this.state.longitude;
      }

      var markers = [{
        position: {
          lat: lat,
          lng: longt,
        }
      }]

      console.log("marker:", this.state)
      const GettingStartedGoogleMap = withGoogleMap(props => {
        return (
          <GoogleMap
            ref={props.onMapLoad}
            defaultZoom={15}
            defaultCenter={{ lat: lat, lng: longt}}
            onClick={props.onMapClick}
          >
              <Router history={browserHistory}>
                  <Route path="/" component={Landing}/>
                  <Route path="home"
			component={HomePage}
			requestPath={this.requestPath}
			setSearchLocation={this.setSearchLocation}
			setShowNearby={this.setShowNearby}
			showNearby={this.state.showNearby}
			landmarks={this.state.landmarks}
			nearbyDirection={this.state.nearbyDirection}
		        directions = {this.state.directions}
		        showDirection = {this.state.showDirection}
		        requestDirection = {this.requestDirection}
		        setShowDirection = {this.setShowDirection}
		  />
                  <Route path="login" component={LoginPage}/>
                  <Route path="signup" component={SignupPage}/>
                  <Route path="resetpassword" component={ResetPasswordPage}/>
                  <Route path="emailsentpage" component={EmailSentPage}/>
                  <Route path="welcome" component={WelcomePage}/>
                  <Route path="newpwd" component={NewPwdPage}/>
                  <Route path="newpwd(/:email)" component={NewPwdPage}/>
                  <Route path="accountsetting" component={AccountSetting}/>
                  <Route path="searchhistory" component={SearchHistory}/>
                  <Route path="myfavourites" component={MyFavourites}/>
                  <Route path="mycontributions" component={MyContributions}/>
              </Router>

            {props.markers.map((marker, index) => (
              <Marker
                key={index}
                position={marker.position}
                onRightClick={() => props.onMarkerRightClick(marker)}
              />
            ))}

	    { props.directions &&
	      <DirectionsRenderer options={{draggable: true}}
		                  ref={(r) => this.directionsRef = r}
		                  directions={props.directions}
		                  onDirectionsChanged={this.onDirectionsChanged}
	      />
	    }
          </GoogleMap>
        )
       }
      )

      return (
        <div style={{height: '100%', width: '100%', position:'absolute'}}>
          <GettingStartedGoogleMap
              containerElement={
                <div style={{height: '100%', width: '100%'}} />
              }
              mapElement={
                <div style={{ height: "100%" }} />
              }
              markers={markers}
	      directions={this.state.directions}
          />
        </div>
      )
    }
}

//export default MapContainer
MapContainer.propTypes = {
    login: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        login: state.login
    };
}

export default connect(mapStateToProps, {})(MapContainer);
