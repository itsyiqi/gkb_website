

/*
 * This component renders Tina's new page
 */
import React, { Component }  from 'react'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import AntPath from "react-leaflet-ant-path";
import ShowDirectionsMap from "./ShowDirectionsMap";



class ShowDirections extends React.Component {
    constructor() {
        super();
        this.state = {
            showDirection: false
        }

    }

    direction(e){
        this.setState({showDirection: !this.state.showDirection})

    }

    

    render() {
       return (
            <div>
                <input></input>
                <button onClick={this.direction.bind(this)} href='#'>Go</button>
                {this.state.showDirection && < ShowDirectionsMap/>}


            </div>

        );
    }
}


export default ShowDirections;
