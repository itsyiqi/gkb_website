/*
 * This component is the search bar, it provides auto suggestions, also search button function.
 * This component imports GoogleMapLoader and GooglePlacesSuggest to implement the core function.
 */

import React, {Component} from "react";
import GoogleMapLoader from "react-google-maps-loader";
import GooglePlacesSuggest from "react-google-places-suggest";
import { connect } from 'react-redux';

const MY_API_KEY = "AIzaSyBUBlaoyXuk4XrvcX7EUzvz-Ri1K9q4Ihs"

class DirectionSuggest extends Component {
    constructor(props){
        super(props);
        this.state = {
		searchStr: "",
		value: "",
        }

        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleSelectSuggest = this.handleSelectSuggest.bind(this);
	this.closeDirection = this.closeDirection.bind(this);
    }

    handleSearchChange(e) {
        this.setState({searchStr: e.target.value})
    }

    handleSelectSuggest(suggest, coordinate, place, directionsResponse) {
        console.log("clicked on text: ", suggest.terms[0].value);
	this.props.requestPath(
	    { lat:parseFloat(coordinate.latitude), lng:parseFloat(coordinate.longitude) }
	);
    }

    closeDirection(e) {
        this.props.setShowDirection(false)
    }

    render() {
        const {searchStr} = this.state;
        const {googleMaps} = this.props;
	const origin = this.props.searchResult["searchResultPageConfig"]["placeFullAddr"];

	var list = ""
	if (this.props.directions && this.props.directions != "") {
		var steps = this.props.directions["routes"][0]["legs"][0]["steps"]
		for (var i = 0; i < steps.length; i++) {
		    list += "&#8226;" + steps[i]["instructions"] + "<br>"
		}
	}

        return (
	<div className="row">
	    <div className="col-lg-3 direction-page-container no-click-through">
		<div className="row">
		    <div className="col-lg-12 direction-page-head">
		        <div className="row">
		            <button
		                type="button"
		                className="btn btn-primary col-lg-12"
		                onClick={this.closeDirection}>
		                    Close
		            </button>
		        </div>

		        <div className="row">
		            <div className="col-lg-12 direction-page-up">
                                <GooglePlacesSuggest
                                    googleMaps={googleMaps}
                                    search={""}
                                >
		                    <div className="input-group search-bar-box">
                                    <input
                                        type="text"
                                        className="form-control"
		                        placeholder={origin}
                                        disabled
                                    />
		                    <span className="input-group-btn">
		                        <button className="btn btn-default" type="button" disabled> > </button>
		                    </span>
	                            </div>
                                </GooglePlacesSuggest>
                            </div>
		        </div>

		        <div className="row">
		            <div className="col-lg-12 direction-page-bottom">
                                <GooglePlacesSuggest
                                    googleMaps={googleMaps}
                                    onSelectSuggest={this.handleSelectSuggest}
                                    search={searchStr}
                                >
		                    <div className="input-group search-bar-box">
                                    <input
                                        type="text"
                                        value={searchStr}
                                        className="form-control"
                                        onChange={this.handleSearchChange}
		                        placeholder={"Direct to..."}
                                    />
		                    <span className="input-group-btn">
		                        <button className="btn btn-default" type="button"
		                             onClick={this.buttonClick}> > </button>
		                    </span>
	                            </div>
                                </GooglePlacesSuggest>
	                    </div>
	                </div>
	            </div>
	        </div>
		<div className="row">
	             <div className="col-lg-12 direction-page-desc" dangerouslySetInnerHTML={{__html: list}} />
	        </div>
	    </div>
	</div>
        )
    }
}

DirectionSuggest.contextTypes = {
    router: React.PropTypes.object.isRequired
}

function mapStateToProps(state) {
    console.log('mapStateToProps: ',state.login);
    return {
    };
}

export default connect(mapStateToProps, null)(GoogleMapLoader(DirectionSuggest, {
    libraries: ["places"],
    key: MY_API_KEY,
}));
