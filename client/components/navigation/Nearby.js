/**
 * Created by TinaAcc on 8/7/17.
 */

/*
 * This component renders Tina's new page
 */
import React from 'react';
import { PlaceService } from 'google-maps-react';
import config from '../../../server/config.js';

class Nearby extends React.Component {

    constructor(props) {
        super(props);
        console.log("Entering Nearby");
	this.refineCoordinate = this.refineCoordinate.bind(this);
    }

    direction(key, e){
      var osm_id = this.props.id;
      var osm_type = this.props.type;

      $.ajax({
         url: 'http://115.146.90.170:8080/direction?location='+osm_type +':'+ osm_id+'&mode=strictdirected&landmarkNumber='+e.id,
         data: {
            format: 'json'
         },
         error: function(xhr,status,error) {
          var err = eval("("+xhr.responseText+ ")");
         },
         context:this,
         success: function(data) {
           this.refineCoordinate(parseFloat(e.lattitude), parseFloat(e.longitude), key, data.direction)
         },
         type: 'GET'
      });
    }

    refineCoordinate(lat, lng, name, direction) {
	var service = new google.maps.places.PlacesService(document.createElement('div'));
	var request = {
	    location: new google.maps.LatLng(lat, lng),
	    radius: '100',
	    name: name,
	};
        service.nearbySearch(
		request,
		(results, status) => {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				var temp = JSON.parse(JSON.stringify(results[0]["geometry"]["location"]))
				lat = temp["lat"]
				lng = temp["lng"]
			}
			this.props.requestPath({lat: lat, lng: lng}, direction)
		}
	)
    }

    render() {
        var obj;
	try {
	    obj = JSON.parse(this.props.landmarks);
	} catch(err) {}

        var list=[];
        for( var key in obj){
          list.push(<li><a onClick={this.direction.bind(this,key, obj[key])} href="#">{key} </a></li>)
        }

        return (
	    <div className="row">
	        <div className="nearby_tag col-md-12 col-lg-12">
		    <div className="row">
                        <h3 className="col-lg-12"><b>Nearby</b></h3>
		    </div>
		    <div className="row">
                        <div className="col-lg-12 landmark-sec" id='LandmarkDirectionResult'>
		            <div>{this.props.nearbyDirection}</div>
                            <ul>{list}</ul>
                        </div>
		    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log('nearby: ',state);
    return {
        login: state.login,
        searchResult: state.searchResult
    };
}
export default Nearby;
