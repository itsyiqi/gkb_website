/**
 * Created by TinaAcc on 8/7/17.
 */

/*
 * This component renders Tina's new page
 */
import React from 'react';


class Service1 extends React.Component {
    constructor() {
        super();
        console.log("Entering service1");
        this.state = {
            OSMInput: '',
            errors: {}
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    isValid() {

        return true;
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            this.setState({errors: {} });
            console.log("Submitting Service1");
            window.location.href = "/direction?location=way:" + document.getElementById("OSMInput").value + "&mode=strictdirected";


        }
    }

    render() {
        return (
            <div className="container float_on_the_map-large">
                <div className="row">
                    <div className="high_row over-map-block">
                        <form onSubmit={this.onSubmit}>
                            <h3>Service 1</h3>
                            <input required id="OSMInput" placeholder="OSMInput"/>
                            <button type="submit"> Submit </button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}


export default Service1;
