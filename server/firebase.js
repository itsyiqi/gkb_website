import * as firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyBazN-o3KXwFJaTeLXOewOBpe1yfDdSp9c",
    authDomain: "myproject-bfeb7.firebaseapp.com"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
