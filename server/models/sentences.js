/**
 * Created by xutianyu on 20/08/2017.
 */

/*
 * sentences contains 3 fields
 * geometry
 * type
 * property
 */

import mongoose from 'mongoose';

var Schema = mongoose.Schema;

var sentencesSchema = new Schema({
    geometry: {type: String},
    type: {type: String},
    properties: {
        sentences: [{type: String, require: true}],
        type: {type: String, require: true},
        name: {type: String, require: true},
        om_id: {type: Number, require: true}
    },
});

module.exports = mongoose.model('Sentence', sentencesSchema, 'sentences');

