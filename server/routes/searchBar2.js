/*
 * This route handles requests for search a place
 * When the request comes in, the sever will search Apache Jena first, if no result, the sever will seach result from Google.
 */

import express from 'express';
import lodash from 'lodash';
import validator from 'validator';
import config from '../config'
import curl from 'curlrequest';
import jwt from 'jsonwebtoken';
import rest from 'rest';
import ElementEl from './../models/node.js';
import User from './../models/user.js';
import DescriptionSchema from './../models/placeDescription';
import GooglePlaces from './../models/googlePlaces';
import autoDescription from './../models/autoDescription.js';
import sentences from './../models/sentences';

var stringSimilarity = require('string-similarity');

let router = express.Router();

router.post('/', (req, res) => {
    console.log(req.body);
    console.log("id: " + req.body.user_id);
    console.log("searchStr: " + req.body.searchStr);
    console.log("finally in searchBar route");
    console.log("fulladdr: ", req.body.fulladdr);
    console.log("DATE: ", Date());

    const query = {placeFullAddr: req.body.fulladdr}

    const button = req.body.button

    // if button was clicked, different way of dealing
    if (button) {
        console.log("go button clicked")
        var results = [];
        // using google text search api, results in json format
        rest('http://freegeoip.net/json/').then(function (response) {
            var parsedData = JSON.parse(response.entity)
            var pos = {
                lat: parsedData.latitude,
                lng: parsedData.longitude
            };

            var encodeRes = encodeURIComponent(req.body.searchStr)

            var url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + encodeRes + "&location=" + pos.lat + "," + pos.lng + "&radius=20&key=" + config.googlePlaceApiKey
            // https://maps.googleapis.com/maps/api/place/textsearch/json?query=coles&location=-37.8103,144.9544&radius=20&key=AIzaSyBYNqtR2RJBsq44d31RZe2Znch8_SX4RXM
            // https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=<>&key=AIzaSyBYNqtR2RJBsq44d31RZe2Znch8_SX4RXM
            console.log("url: ", url)
            var options = {url: url};
            var errors = {}
            curl.request(options, function (err, res1) {
                if (err) {
                    errors.searchBar = "google api error, We could not find " + req.body.searchStr
                    res.status(400).json(errors);
                } else {
                    var element = JSON.parse(res1)
                    if (!element.results[0]) {
                        console.log("not in google search")
                        errors.searchBar = "We could not find " + req.body.searchStr
                        res.status(400).json(errors);
                    } else {
                        for (var i = 0; i < element.results.length; i++) {
                            var elem = element.results[i];
                            console.log("element: ", elem)
                            var lat = element.results[i].geometry.location.lat;
                            var lng = element.results[i].geometry.location.lng;
                            var addr = elem.formatted_address;
                            console.log("addr111: ", addr)
                            var name = elem.name;
                            if (elem.photos) {
                                var photo_ref = elem.photos[0].photo_reference;
                            } else {
                                var photo_ref = null
                            }
                            var obj = {
                                lat: lat,
                                lng: lng,
                                photo: photo_ref,
                                addr: addr,
                                name: name
                            }
                            // console.log("photo: ", res2);
                            console.log("obj addr: ", obj.addr)
                            results.push(obj)
                            console.log("results array: ", results.length);
                            if (results.length == element.results.length) {
                                console.log("its time to return to client side")
                                res.json({results});
                            }

                        }
                    }

                }
            });

        });

    } else {
        console.log("clicked on google Auto suggestion");
        console.log(req.body)
        var ret = {};
        ret = queryJena(req.body.searchStr, req.body.fulladdr, req.body.user_id, req.body.coord, function (ret) {
            console.log("ret: ", ret)
            if (ret.error == 1) {

                console.log("not in jena, but in google");
                var errors = ret.errors;
                const query = {placeFullAddr: req.body.fulladdr}
                User.findById(req.body.user_id, function (err, s_user) {
                    DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                        if (err) return handleError(err);
                        //console.log(docs);
                        var counter = 1
                        var descriptionArray = [];
                        //console.log(docs);
                        if (docs.length == 0) {
                            res.status(400).json({
                                errors: null,
                                searchHistory: ret.searchHistory,
                                descriptionArray: null,
                                favorites: null
                            });
                        } else {
                            docs.forEach((doc) => {
                                //console.log(doc);
                                var temp = {};
                                //console.log("user_id: ", doc);
                                User.findById(doc.user_id, 'proImg', function (err, user) {
                                    temp.doc = doc;
                                    temp.proImg = user.proImg;
                                    //console.log("temp   ", temp);
                                    descriptionArray.push(temp);
                                    if (counter == docs.length) {
                                        descriptionArray.sort((a, b) => {
                                            if (a.doc.like > b.doc.like) {
                                                return -1;
                                            } else if (a.doc.like < b.doc.like) {
                                                return 1;
                                            }
                                            return 0;
                                        });
                                        var errors = {
                                            errors: ret.errors,
                                            searchHistory: ret.searchHistory,
                                            descriptionArray: descriptionArray,
                                            favorites: ret.favorites
                                        }
                                        console.log("not in jena, but in google. With descriptionArray", errors)
                                        res.status(400).json(errors);
                                    }
                                    counter += 1;
                                });
                            });
                        }
                    });
                });

            } else {
                var token = ret.token;
                console.log("ret",ret);
                console.log("return from jena");

                const query = {placeFullAddr: token.placeFullAddr};
                User.findById(req.body.user_id, function (err, s_user) {
                    DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                        if (err) return handleError(err);
                        //console.log(docs);
                        var counter = 1
                        var descriptionArray = [];
                        //console.log(docs);
                        if (docs.length == 0) {
                            console.log(" in jena, Without descriptionArray")
                            token.descriptionArray = null
                            res.json({token});
                        } else {
                            docs.forEach((doc) => {
                                //console.log(doc);
                                var temp = {};
                                console.log("user_id: ", doc);
                                User.findById(doc.user_id, 'proImg', function (err, user) {
                                    temp.doc = doc;
                                    temp.proImg = user.proImg;
                                    //console.log("temp   ", temp);
                                    descriptionArray.push(temp);
                                    //console.log(temp);
                                    if (counter == docs.length) {
                                        //console.log("All done")
                                        descriptionArray.sort((a, b) => {
                                            if (a.doc.like > b.doc.like) {
                                                return -1;
                                            } else if (a.doc.like < b.doc.like) {
                                                return 1;
                                            }
                                            return 0;
                                        });

                                        console.log(" in jena, With descriptionArray");
                                        token.descriptionArray = descriptionArray;
                                        res.json({token});
                                    }
                                    counter += 1;
                                });
                            });
                        }
                    });
                });

            }
        });
    }
});

function tempfunc(options2, photo_ref, addr, lat, lng, name, callback) {
    if (photo_ref) {
        curl.request(options2, function (err, res2) {
            if (err) {
                console.log("photo ref error")
            } else {
                console.log("photo url available")
                var obj = {
                    photo: photo_ref,
                    addr: addr,
                    lat: lat,
                    lng: lng,
                    name: name
                }
                callback(obj)
            }
        });
    } else {
        var obj = {
            photo: null,
            addr: addr,
            lat: lat,
            lng: lng,
            name: name
        }
        callback(obj)
    }
}

function querySentence(placeid , temp , callback ){

    var sentence;
    // add by justin , query sentences
    //query = sentences.findOne({'properties.om_id': placeid});

    //return query;
    temp = sentence;
    console.log("inside temp", temp);

    sentences.findOne({'properties.om_id': placeid}, function (err, res2) {
        if (err) {
            console.log("err in sentences findone");
            console.log("error ", err);
            console.log("sentence res", res2);
        }
        else {
            if (res2) {
                sentence = res2.token;
                temp = res2.token;
                console.log("res sentences", res2);
            }
            else {
                return null;
                console.log("in sentencce err");
            }
        }
        const token ={
            sentence: res2.token
        }
        var ret = {
            error: 0,
            token: token
        }

        callback(ret);
    });


}
/*
function callNominatim(searchStr, fulladdr, coord, callback){
    var lat = coord['latitude']
    var lon = coord['longitude']
    console.log("LAT: " + lat);
    console.log("LON: " + lon);
    console.log("TITLE: " + coord['title']);
    //var temp = 'http://130.220.209.141/nominatim/reverse?lat='+lat+'&lon='+lon
    var temp = 'http://130.220.209.141/nominatim/search?bounded=1&viewbox='+(lon-1)+','+(lat-1)+','+(lon+1)+','+(lat+1)+'&q='
    var temp2 = '&format=json&addressdetails=1'
    var encodeRes = encodeURIComponent(coord['title'])
    var url = temp + encodeRes + temp2
    var options = {url: url};
    console.log("encodeRes: " + encodeRes)
    console.log("url: " + url);

    curl.request(options, function (err, res1) {
        //console.log("RES1")
        //console.log(res1)
        if(err){
            console.log("in err curl req");
        }
        else{
            var found1 = false;
            try
            {
                var element1 = JSON.parse(res1)
                for (var i = 0; i < element1.length; i++) {
                    if(element1[i].display_name.indexOf(searchStr) !== -1){
                        found1 = true;
                    }
                }
                if(res1 == '[]' || !found1){
                    encodeRes = encodeURIComponent(fulladdr)
                    url = temp + encodeRes + temp2
                    options = {url: url};
                    console.log("encodeRes: " + encodeRes)
                    console.log("url: " + url);
                    curl.request(options, function (err, res2) {
                        if(err){
                            console.log("in err curl req");
                        }
                        else{
                            var found2 = false;
                            var element2 = JSON.parse(res2)
                            for (var i = 0; i < element2.length; i++) {
                                if(element2[i].display_name.indexOf(searchStr) !== -1){
                                    found2 = true;
                                }
                            }
                            if(res2 == '[]' || !found2){
                                encodeRes = encodeURIComponent(searchStr)
                                url = temp + encodeRes + temp2
                                options = {url: url};
                                console.log("encodeRes: " + encodeRes)
                                console.log("url: " + url);
                                curl.request(options, function (err, res3) {
                                    if(err){
                                    }
                                    else{
                                        callback(res3);
                                    }
                                });
                            }
                            else{
                                callback(res2);
                            }
                        }
                    });
                }
                else{
                    callback(res1);
                }
            }
            catch(err){

            }
        }
    });
}*/

function callNominatim(searchStr, fulladdr, coord, callback){
    var lat = coord['latitude']
    var lon = coord['longitude']
    console.log("LAT: " + lat);
    console.log("LON: " + lon);
    console.log("TITLE: " + coord['title']);
    //var temp = 'http://130.220.209.141/nominatim/reverse?lat='+lat+'&lon='+lon
    var temp = 'http://130.220.209.141/nominatim/search?bounded=1&viewbox='+(lon-0.001)+','+(lat-0.001)+','+(lon+0.001)+','+(lat+0.001)+'&q='
    var temp2 = '&format=json&addressdetails=1'
    var encodeRes = encodeURIComponent(searchStr)
    var url = temp + encodeRes + temp2
    var options = {url: url};
    console.log("encodeRes: " + encodeRes)
    console.log("url: " + url);

    curl.request(options, function (err, res1) {
        //console.log("RES1")
        //console.log(res1)
        if(err){
            console.log("in err curl req");
        }
        else{
            callback(res1);
        }
    });
}

function queryJena(searchStr, fulladdr, id, coord,callback) {

    var nominatim_result = null
    callNominatim(searchStr, fulladdr, coord,function(ret){
        nominatim_result = ret;
        console.log("nominatim result");
        console.log(nominatim_result);
        let errors = {};
        var element = JSON.parse(nominatim_result)
        if (!element[0]) {
            console.log("not in nominatim jena service")
            errors.searchBar = "We could not find " + fulladdr
            // Then store the searchstr in searchStr in db with google type.
            if (id != null) {
                User.findOne({_id: id}, function (err, data2) {
                    let errors = {};
                    var searchHistoryStore = [];
                    // console.log("data2: " + data2);
                    searchHistoryStore = data2.searchHistory;
                    // console.log("searchHistoryStore: ", searchHistoryStore)
                    if (err) {
                        console.log(err);
                    } else if (!data2) {
                        //errors.login = "Email does not exist or wrong password";
                        //res.status(400).json(errors);
                    } else {
                        var insertToSearchHistory = {
                            type: "google",
                            searchStr: fulladdr,
                        }
                        //Update searchHistory in user Model.
                        User.update(
                            {_id: id, searchHistory: insertToSearchHistory},
                            {$addToSet: {searchHistory: insertToSearchHistory}},
                            function (err, user) {
                                if (err) {
                                    console.log("error in searchhistory update");
                                } else {

                                    User.findOne({
                                        _id: id,
                                        'searchHistory.searchStr': fulladdr
                                    }, function (err, data) {

                                        if (err) {
                                            console.log("1111111111111 google");
                                        } else if (!data) {
                                            console.log("2222222222222 google");
                                            var insertToSearchHistoryNew = {
                                                type: "google",
                                                searchStr: fulladdr,
                                                date: new Date()
                                            }
                                            User.update(
                                                {_id: id},
                                                {$addToSet: {searchHistory: insertToSearchHistoryNew}},
                                                function (err, user) {
                                                    if (err) {
                                                        console.log("in 2nd update error")
                                                    } else {
                                                        console.log("in 2nd update success");

                                                        searchHistoryStore.push(insertToSearchHistoryNew);
                                                        console.log("in searchHistoryStore adding new location: ", searchHistoryStore)
                                                        var ret = {
                                                            error: 1,
                                                            errors: errors,
                                                            searchHistory: searchHistoryStore,
                                                            autoDescription: null,
                                                            favorites: data2.favorites,
                                                            osm_id:nominatim_result.place_id,
                                                            osm_type: nominatim_result.osm_type
                                                        }
                                                        callback(ret);
                                                    }
                                                })

                                        } else {
                                            console.log("333333333333333 google")
                                            User.update(
                                                {'searchHistory.searchStr': fulladdr},
                                                {$set: {'searchHistory.$.date': new Date()}},
                                                function (err, user2) {
                                                    if (err) {
                                                        console.log("error date updated");
                                                    } else {
                                                        console.log("updating date", user2);
                                                        var ret = {
                                                            error: 1,
                                                            errors: errors,
                                                            searchHistory: data2.searchHistory,
                                                            autoDescription: null,
                                                            favorites: data2.favorites,
                                                            osm_id:nominatim_result.place_id,
                                                            osm_type: nominatim_result.osm_type

                                                        }
                                                        callback(ret);
                                                    }

                                                })
                                        }
                                    })

                                }
                            });
                    }
                });
            } else {
                // no user. Guest user trying to search
                console.log("In google searchbar line 312, in else part of no user.");
                errors.searchBar = "We could not find " + searchStr

                var ret = {
                    error: 1,
                    errors: errors,
                    searchHistory: null,
                    autoDescription: null,
                    favorites: null
                }
                callback(ret);
            }

        } else {

            var compare = searchStr + " " + fulladdr;
            var bestMatch = stringSimilarity.compareTwoStrings(element[0].display_name, compare);
            var idxBest = 0
            console.log(element[0].display_name)
            console.log(compare)
            console.log("SIM: ", bestMatch);
            console.log("\n\n\n");
            for (var i = 1; i < element.length; i++) {
                console.log(element[i].display_name)
                console.log(compare)
                var tmp = stringSimilarity.compareTwoStrings(element[i].display_name, compare);
                if (tmp > bestMatch) {
                    bestMatch = tmp;
                    idxBest = i;
                }
                console.log("SIM: ", tmp);
                console.log("\n\n\n");
            }

            console.log("jena success");
            // var element = JSON.parse(res1)
            console.log("element ", element[idxBest])
            //console.log("res1[0]: ",res1[0])
            var elem = element[idxBest]
            console.log("coords lat: ", elem.lat)
            console.log("coords longt: ", elem.lon)
            console.log("osm_id: ", elem.osm_id)

            fulladdr = elem.display_name;

            // add by justin
            /*
            var sentence = {};
            var ret = {};
            var tmp = {};
            ret = querySentence(elem.osm_id, tmp, function(err, ret){
                //temp = ret;
                console.log("inside ret", ret);

            });
            console.log ("outside temp", tmp);
            */


            autoDescription.findOne({'element': elem.osm_id}, function (err, res) {
                if (err) {
                    console.log("err in autoDescription findone")
                } else {


                    console.log("no err in autodescription");
                    console.log("res: ", res);
                    var autoDescription;



                    //res.autoDescription = res.autoDescription + sentence;


                    if (res) {
                        console.log("res autoDescription:",res);
                        autoDescription = res.autoDescription ;
                        console.log("in doc success")
                    } else {
                        autoDescription = null;
                        console.log("in doc err")
                    }
                    if (id != null) {
                        User.findOne({_id: id}, function (err, data2) {
                            var searchHistoryStore = [];
                            let errors = {};
                            // console.log("data2: " + data2);
                            searchHistoryStore = data2.searchHistory;
                            if (err) {
                                console.log(err);
                            } else if (!data2) {
                                //errors.login = "Email does not exist or wrong password";
                                //res.status(400).json(errors);
                            } else {
                                var coords2 = {
                                    lat: elem.lat,
                                    longt: elem.lon
                                }
                                console.log("coords2: ", coords2);


                                var insertToSearchHistory = {
                                    type: "jena",
                                    element: elem.osm_id,
                                    searchStr: fulladdr,
                                    //date: new Date()
                                }
                                //Update searchHistory in user Model.
                                User.update(
                                    {_id: id, searchHistory: insertToSearchHistory},
                                    {$addToSet: {searchHistory: insertToSearchHistory}},
                                    function (err, user) {
                                        if (err) {
                                            console.log("error in searchhistory update");
                                        } else {
                                            console.log("succes in updataing searchHistory11111");


                                            User.findOne({
                                                _id: id,
                                                'searchHistory.searchStr': fulladdr
                                            }, function (err, data) {

                                                if (err) {
                                                    console.log("1111111111111");
                                                } else if (!data) {
                                                    console.log("2222222222222");
                                                    var insertToSearchHistoryNew = {
                                                        type: "jena",
                                                        element: elem.osm_id,
                                                        searchStr: fulladdr,
                                                        date: new Date()
                                                    }
                                                    User.update(
                                                        {_id: id},
                                                        {$addToSet: {searchHistory: insertToSearchHistoryNew}},
                                                        function (err, user) {
                                                            if (err) {
                                                                console.log("in 2nd update error")
                                                            } else {
                                                                searchHistoryStore.push(insertToSearchHistoryNew);
                                                                // console.log("in searchHistoryStore adding new location: ", searchHistoryStore)

                                                                console.log("in 2nd update success");
                                                                const token = {
                                                                    email: data2.email,
                                                                    userName: data2.userName,
                                                                    accountType: data2.accountType,
                                                                    _id: data2._id,
                                                                    proImg: data2.proImg,
                                                                    coords: coords2,
                                                                    placeFullAddr: fulladdr,
                                                                    placePhoto: "",
                                                                    searchHistory: searchHistoryStore,
                                                                    autoDescription: autoDescription,
                                                                    favorites: data2.favorites,
                                                                    osm_id:elem.osm_id,
                                                                    osm_type: elem.osm_type
                                                                    // showSearchResult: true
                                                                }
                                                                console.log("search bar sending token ");
                                                                //res.json({token});
                                                                var ret = {
                                                                    error: 0,
                                                                    token: token
                                                                }
                                                                callback(ret);
                                                            }
                                                        })

                                                } else {
                                                    console.log("333333333333333")
                                                    User.update(
                                                        {'searchHistory.searchStr': fulladdr},
                                                        {$set: {'searchHistory.$.date': new Date()}},
                                                        function (err, user2) {
                                                            if (err) {
                                                                console.log("error date updated");
                                                            } else {
                                                                // console.log("updating date",  user2);
                                                                const token = {
                                                                    email: data2.email,
                                                                    userName: data2.userName,
                                                                    accountType: data2.accountType,
                                                                    _id: data2._id,
                                                                    proImg: data2.proImg,
                                                                    coords: coords2,
                                                                    placeFullAddr: fulladdr,
                                                                    placePhoto: "",
                                                                    searchHistory: data2.searchHistory,
                                                                    autoDescription: autoDescription,
                                                                    favorites: data2.favorites,
                                                                    osm_id:elem.osm_id,
                                                                    osm_type: elem.osm_type
                                                                    // showSearchResult: true
                                                                }
                                                                console.log("search bar sending token ");
                                                                //res.json({token});
                                                                var ret = {
                                                                    error: 0,
                                                                    token: token
                                                                }
                                                                callback(ret);
                                                            }

                                                        })
                                                }
                                            })

                                        }
                                    });
                            }
                        });

                        //});
                    } else {
                        /*
                        var coords2 = {
                            lat: elem.lat,
                            longt: elem.lon
                        }
                        console.log("coords2: ", coords2);
                        const token = {
                            email: null,
                            userName: null,
                            accountType: null,
                            _id: null,
                            proImg: null,
                            coords: coords2,
                            placeFullAddr: fulladdr,
                            placePhoto: "",
                            searchHistory: null,
                            autoDescription: autoDescription,
                            //sentence: sentence,
                            favorites: null
                            // showSearchResult: true
                            // }, 'secretkeyforjsonwebtoken');
                        }
                        console.log("search bar sending token2 ");
                        //res.json({token});
                        var ret = {
                            error: 0,
                            token: token

                        }
                        callback(ret);
                        */

                        // add sentence

                        var coords2 = {
                            lat: elem.lat,
                            longt: elem.lon
                        }
                        console.log("coords2: ", coords2);

                        sentences.findOne({'properties.om_id': elem.osm_id}, function (err, data3) {
                            if (err) {
                                console.log("err in sentences findone");
                                console.log("error ", err);
                                console.log("sentence res", data3);

                                const token = {
                                    email: null,
                                    userName: null,
                                    accountType: null,
                                    _id: null,
                                    proImg: null,
                                    coords: coords2,
                                    placeFullAddr: fulladdr,
                                    placePhoto: "",
                                    searchHistory: null,
                                    autoDescription: autoDescription,
                                    //sentence: sentence,
                                    favorites: null,
                                    // showSearchResult: true
                                    // }, 'secretkeyforjsonwebtoken');
                                    osm_id:elem.osm_id,
                                    osm_type: elem.osm_type
                                }
                                console.log("search bar sending token2 ");
                                //res.json({token});
                                var ret = {
                                    error: 0,
                                    token: token

                                }
                                callback(ret);
                            }
                            else if (!data3){
                                const token = {
                                    email: null,
                                    userName: null,
                                    accountType: null,
                                    _id: null,
                                    proImg: null,
                                    coords: coords2,
                                    placeFullAddr: fulladdr,
                                    placePhoto: "",
                                    searchHistory: null,
                                    autoDescription: autoDescription,
                                    sentence: null,
                                    favorites: null,
                                    osm_id:elem.osm_id,
                                    osm_type:elem.osm_type
                                    // showSearchResult: true
                                    // }, 'secretkeyforjsonwebtoken');
                                }
                                console.log("search bar sending token2 ");
                                //res.json({token});
                                var ret = {
                                    error: 0,
                                    token: token

                                }
                                callback(ret);
                            }
                            else {
                                //autoDescription = autoDescription + data3.properties.sentences;
                                var autoSentence;
                                //var index =0;
                                if(autoDescription.includes(data3.properties.sentences[0])){
                                    console.log("sentence is included");
                                    if(data3.properties.sentences.length >= 2){
                                        autoSentence = data3.properties.sentences[1];
                                    }
                                    else {
                                        autoSentence = null;
                                    }
                                }
                                else {
                                    autoSentence = data3.properties.sentences[0];

                                }

                                const token = {
                                    email: null,
                                    userName: null,
                                    accountType: null,
                                    _id: null,
                                    proImg: null,
                                    coords: coords2,
                                    placeFullAddr: fulladdr,
                                    placePhoto: "",
                                    searchHistory: null,
                                    autoDescription: autoDescription,
                                    sentence: autoSentence,
                                    //sentence: sentence,
                                    favorites: null,
                                    osm_id:elem.osm_id,
                                    osm_type: elem.osm_type
                                    // showSearchResult: true
                                    // }, 'secretkeyforjsonwebtoken');
                                }
                                console.log("search bar sending new token2 ");
                                //res.json({token});
                                var ret = {
                                    error: 0,
                                    token: token

                                }
                                callback(ret);
                            }


                            //callback(ret);
                        });


                    }
                }
            });

        }
    });
}


// handle search_bar location search
// test version
router.get('/testgo', (req, res) => {
    //console.log('msg from testgo:');
    //console.log(req.query.location);
    const resultlist = loadResultList(req.query.location);
    console.log(resultlist);
    //const token = jwt.sign(resultlist, 'secretkeyforjsonwebtoken');
    //res.json(resultlist);
    //res.json({users: 'users'});
    //res.send('hello');
    //res.json(resultlist);
    res.json(resultlist);

    // console.log("TestGO:" + resultlist);
    // console.log("TestGO:" + resultlist.location);
    // console.log("TestGO:" + resultlist.resultArray);
});

router.post('/addDescription', (req, res) => {

    // Add this place to googlePlaces in db only if they type is google
    if (req.body.type == "google") {
        console.log("in addDescription google places add");

        var query = {
            addr: req.body.placeFullAddr,
        }

        var place = {
            addr: req.body.placeFullAddr,
            image: req.body.image,
            coords: req.body.coords,
            // date: Date(),
        }

        GooglePlaces.find(query).count(function (err, count) {
            let errors = {}
            console.log("In addFavorites Number of docs: ", count);
            if (count === 0) {
                GooglePlaces.create(place, function (err, dataGoogle) {
                    console.log("Writing to db");
                    if (err) {
                        console.log(err.statusCode);
                    } else if (!dataGoogle) {
                        console.log(res.statusCode);
                        console.log("Error saving");
                    } else {
                        console.log(res.statusCode);
                        console.log("Registered");
                    }

                });
            } else {
                console.log("place already exists in Favorites db");
                // errors.signup = "place already exists in Favorites db";
                // res.status(400).json(errors);
            }
        });


    }

    var description = req.body
    console.log("descriptionis gonna be stored.: ", description)
    // description.date = new Date().Format("yyyy-MM-dd HH:mm:ss");
    description.like = 0;
    description.date = Date();
    // console.log(description);
    // console.log(description);


    DescriptionSchema.create(description, function (err, data) {
        console.log("Writing to db");
        console.log(description);
        if (err) {
            console.log(err.statusCode);
            console.log(err);
            console.log("get error");
        } else if (!data) {
            console.log(res.statusCode);
            console.log("Error saving");
        } else {
            console.log(res.statusCode);
            console.log("Description added!!!!");
            const query = {placeFullAddr: description.placeFullAddr};
            //console.log(query);

            // DescriptionSchema.find(query, 'user_email placeFullAddr like', function (err, place) {
            //     if (err) return handleError(err);
            //     console.log('%s %s %s.', place.user_email, place.placeFullAddr, place.like) // Space Ghost is a talk show host.
            // });
            DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                if (err) console.log(err);
                var counter = 1
                var descriptionArray = [];
                docs.forEach((doc) => {
                    //console.log(doc);
                    var temp = {};
                    User.findById(doc.user_id, 'proImg', function (err, user) {
                        temp.doc = doc;
                        console.log("user: ***************", user);
                        if (user.proImg == null) {
                            temp.proImg = null
                        } else {
                            temp.proImg = user.proImg;
                        }
                        descriptionArray.push(temp);
                        //console.log(temp);
                        if (counter == docs.length) {
                            //console.log("All done")
                            //console.log(descriptionArray);
                            descriptionArray.sort((a, b) => {
                                if (a.doc.like > b.doc.like) {
                                    return -1;
                                } else if (a.doc.like < b.doc.like) {
                                    return 1;
                                }
                                return 0;
                            });

                            ////////////////////////////////////
                            ////Update contribution here////////
                            ////////////////////////////////////

                            var this_user = {
                                user_id: description.user_id,
                                user_name: description.user_name
                            }
                            console.log("Im this user:" + JSON.stringify(description));

                            DescriptionSchema.find(this_user, function (err, data) {
                                var addresses = [];
                                if (err) {
                                    console.log(err);
                                } else if (!data) {
                                    console.log("No contribution yet");
                                } else {
                                    console.log("Descriptions: " + data[0]);
                                    var user_descriptions = [];
                                    for (var i = 0; i < data.length; i++) {
                                        var this_description = {
                                            location: data[i].placeFullAddr,
                                            description: data[i].description_content,
                                            create_date: data[i].date
                                        }
                                        addresses.push(data[i].placeFullAddr);
                                        user_descriptions.push(this_description);
                                        // console.log("Pushed "+user_descriptions.length);
                                    }

                                }


                                GooglePlaces.find({addr: {$in: addresses}}, function (err, data) {
                                    if (err) {
                                        console.log("Error finding google places " + err);
                                    } else if (!data) {
                                        console.log("Cannot find in googleplaces");
                                    } else {
                                        console.log("MATCHING google palce " + data.length);
                                        for (var i = 0; i < data.length; i++) {
                                            for (var j = 0; j < user_descriptions.length; j++) {
                                                if (data[i].addr === user_descriptions[j].location) {
                                                    user_descriptions[i].image = data[i].image;
                                                }
                                            }

                                        }
                                    }


                                    res.status(200).json({
                                        descriptionArray: descriptionArray,
                                        contributionArray: user_descriptions
                                    });

                                });
                            });

                        }
                        counter += 1;
                    });
                });
            });

        }
    })

});

router.post('/updateContribution', (req, res) => {
    console.log("UPDATING CONTRIBUTION ARRAY IN HERE 1");
    res.status(200).json({});

});


router.post('/addLike', (req, res) => {
    const {des_id} = req.body;
    const {user_id} = req.body;
    // check if the user already liked this one
    var liked = false;
    DescriptionSchema.findById(des_id, function (err, description) {
        const {user_like_array} = description;
        console.log(description);
        var response = {};
        if (user_like_array.indexOf(user_id) === -1) {
            // user haven't like this one
            DescriptionSchema.findByIdAndUpdate(des_id, {
                $inc: {like: 1},
                $push: {user_like_array: user_id}
            }, {new: true}, function (err, description) {
                if (err) return handleError(err);
                // return accepted as signal
                ////////////
                ////////////
                ////////////
                ////////////
                const query = {placeFullAddr: description.placeFullAddr};
                console.log("query", query);
                DescriptionSchema.find(query, '_id user_name user_id description_content like user_like_array', function (err, docs) {
                    if (err) console.log(err);
                    var counter = 1
                    var descriptionArray = [];
                    docs.forEach((doc) => {
                        var temp = {};
                        User.findById(doc.user_id, 'proImg', function (err, user) {
                            temp.doc = doc;
                            if (user.proImg == null) {
                                temp.proImg = null
                            } else {
                                temp.proImg = user.proImg;
                            }
                            descriptionArray.push(temp);
                            if (counter == docs.length) {
                                descriptionArray.sort((a, b) => {
                                    if (a.doc.like > b.doc.like) {
                                        return -1;
                                    } else if (a.doc.like < b.doc.like) {
                                        return 1;
                                    }
                                    return 0;
                                });
                                console.log("return with description array")
                                response.descriptionArray = descriptionArray
                                res.status(200).json(response);
                            }
                            counter += 1;
                        });
                    });
                });
            });
        } else {
            // user already liked this one
            response.ans = false;
            // return refused as signal
            res.status(200).json(response);
        }
    });

});


// simulate load query result from db
function loadResultList(location) {
    const results = {};
    const resultNum = rnd(3, 8);
    const resultArray = []
    for (let i = 0; i < resultNum; i++) {
        let tempJson = {};
        tempJson.userName = 'user-' + i;
        tempJson.rank = rnd(0, 300);
        tempJson.discription = 'rank' + i + 'xxxxxxxxxxxxxxxxxx';
        resultArray.push(tempJson);
    }
    resultArray.sort(function (a, b) {
        return parseInt(b.rank) - parseInt(a.rank);
    });
    results.location = location;
    results.resultArray = resultArray;
    results.autoComment = 'autoComment is here';

    return results;
}

// produce a random number
function rnd(start, end) {
    return Math.floor(Math.random() * (end - start) + start);
}

//we need to get data from post request

export default router;
